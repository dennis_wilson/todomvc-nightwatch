'use strict';

const pleaseImplementMe = require('../lib/please-implement-me.function');

module.exports = {
  'The User can visit the Todo application': testUserCanOpenTheApplication
  'The User can see the application headline': pleaseImplementMe,
  'The User can see an inline textfield for creating new todo list items': pleaseImplementMe,
  'The User can see the "What needs to be done?" caption in the empty input field': pleaseImplementMe
};

function testUserCanOpenTheApplication(browser) {
  browser.url(browser.launch_url, function() {
    browser
      .waitForElementVisible('body', browser.globals.element_timeout_at)
      .end();
  });
}
