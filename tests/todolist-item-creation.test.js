'use strict';

const pleaseImplementMe = require('../lib/please-implement-me.function');

module.exports = {
  // item creation
  'The User can type text into the todo input field': pleaseImplementMe,
  'The User CANNOT see the toggle all icon in the new-todo-input, when there are no todo items saved.': pleaseImplementMe,
  'The User can create a new todo item, by pressing ENTER key when inside the populated todo input field': pleaseImplementMe,
  'The User can create a new todo item, which has the same text as she entered during the creation': pleaseImplementMe,
  'The User can NOT create a new todo, when the todo input field is empty': pleaseImplementMe,
  'The User CAN see the toggle all icon in the new-todo-input, if there is at least ONE todo item saved.': pleaseImplementMe,
  'After the user created a new todo item, the input field is empty and placeholder captioned again.': pleaseImplementMe,
  'Todos a User saved, are available even after reloading the browser': pleaseImplementMe
};


