'use strict';

const pleaseImplementMe = require('../lib/please-implement-me.function');

module.exports = {
  'If the User has no todo item in her list (completed or incomplete), she wont see the todolist footer': pleaseImplementMe,
  'If the User has a at least one todo item in her list (completed or incomplete), she can see the todolist footer': pleaseImplementMe,
  'The User can see in the footer, how many incomplete todolist items she has in her list.': pleaseImplementMe,
  'The User can see three filter options in the list footer by default: all, active and completed.': pleaseImplementMe,
  'The User can click on a list filter from the footer to make the filter button become marked as active': pleaseImplementMe,
  'When all times, there must be at least one filter from the footer being marked as active.': pleaseImplementMe,
  'When the User selects a the "All" filter from the footer, the list displays completed and incomplete todolist items': pleaseImplementMe,
  'When the User selects a the "Active" filter from the footer, the list displays incomplete todolist items only': pleaseImplementMe,
  'When the User selects a the "Completed" filter from the footer, the list displays completed todolist items only': pleaseImplementMe,
  'When the User marks at least one todolist item as complete, there will be a button in the footer which is named "Clear completed"': pleaseImplementMe,
  'When User has no completed todolist items as, she cannot see the "Clear completed" button.': pleaseImplementMe,
  'When the User clicks the "Clear completed" button, all completed todolist items will be removed.': pleaseImplementMe,
};

