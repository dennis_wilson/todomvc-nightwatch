'use strict';

const pleaseImplementMe = require('../lib/please-implement-me.function');

module.exports = {

  // editing todolist item test
  'The User can enable edit mode on an existing todolist item by double clicking on the todo list item.': pleaseImplementMe,
  'The User can change the text of an todo list item which is in edit mode.': pleaseImplementMe,
  'The User can save the text of a todo list item which is in edit mode, by pressing enter after making changes.': pleaseImplementMe,
  'The User can save the text of a todo list item which is in edit mode, by loosing focus of the field after making changes.': pleaseImplementMe,
  'The User can discard the changes she made to a todo list item in edit mode, by pressing the ESC key.': pleaseImplementMe,
  'If the User edits a todo list item again, she edited and discarded before, the edit input field value is the pristine again.': pleaseImplementMe,

  // change todolist item states
  'The User can see an unchecked toggle checkbox on a new created todolist item': pleaseImplementMe,
  'The User cannot see the destroy button on a todolist item by default': pleaseImplementMe,
  'The User cannot see the destroy button on a todolist item she is currently editing': pleaseImplementMe,
  'The User can see the destroy button on a todolist item, that she hovers the mouse over': pleaseImplementMe,
  'The User can mark a todolist item as completed by clicking the todolist item toggle checkbox on an incomplete item': pleaseImplementMe,
  'The User can mark a todolist item as incomplete by clicking the todolist item toggle checkbox on a completed item': pleaseImplementMe,

  // deletion of items
  'The User can delete a completed todolist item by clicking the destroy button on the particular item': pleaseImplementMe,
  'The User can delete a incompleted todolist item by clicking the destroy button on the particular item': pleaseImplementMe
};

